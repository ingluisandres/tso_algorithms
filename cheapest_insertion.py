__license__ = ""
__author__ = "Luis Andrés García Contreras"
__startdate__ = "2/12/2020"
__name__ = "TSO_Algorithms "
__module__ = "cheapest_insertion"
__python_version__ = "3.8.5"

__lastdate__ = ""
__version__ = ""


__all__ = ['']


# Modules -------------------------------------------------------------------------
import math

# Classes


class TSP():

    def __init__(self, file_path, initial_client):
        self.clients = self.getPoints(file_path)
        self.matrix = self.getDistanceMatrix(self.clients)
        self.cheapest = self.cheapest_insertion(initial_client)

    def getPoints(self, file):
        """Function that reads a txt file and gets the coordinates of the clients"""

        clients_points = []
        f = open(file, "r")

        # Read clients's number
        n = int(f.readline())

        # Read locations of all clients
        for i in range(n):
            temp = f.readline()
            temp = temp.replace("\n", "")
            temp = temp.split(" ")
            temp = tuple(temp)
            clients_points.append(temp)

        f.close()

        return clients_points

    def getDistanceMatrix(self, clients_points):
        """Function that with the coordinates of the clients, makes the distance matrix"""

        tabla_de_distancias = []
        for i in range(len(clients_points)):
            temp = []
            for j in range(len(clients_points)):

                if(clients_points[i] == clients_points[j]):
                    temp.append(0)
                else:
                    temp.append(distance(clients_points[i], clients_points[j]))

            tabla_de_distancias.append(temp)

        return tabla_de_distancias

    def getMinusDelta(self):
        """Function that gets the minor delta"""

        minimum = 1000

        for j in range(len(self.cheapest)):
            for i in range(len(self.missing)):

                temp = round(self.matrix[self.cheapest[j][0]][self.missing[i]] +
                             self.matrix[self.missing[i]][self.cheapest[j][0]] -
                             self.matrix[self.cheapest[j][0]][self.cheapest[j][1]], 3)

                if(temp < minimum):
                    minimum = temp
                    n = self.cheapest[j][0]
                    m = self.missing[i]
                    z = j
                    y = i

        self.cheapest.remove(self.cheapest[z])
        self.cheapest.insert(z, [n, m])
        self.cheapest.insert(z+1, [m, self.cheapest[0][0]])
        self.missing.remove(self.missing[y])

        return self.cheapest, self.missing

    def getTotalCost(self):
        """Function that calculates the total cost of a route"""

        cost = 0

        for i in range(len(self.cheapest)):
            cost += self.matrix[self.cheapest[i][0]][self.cheapest[i][1]]

        return round(cost, 3)

    def printRoute(self):
        """Function that prints the actual route"""

        cadena = ""

        for i in range(len(self.cheapest)-1):
            # + str(self.cheapest[i][1])  + "-"
            cadena += str(self.cheapest[i][0]+1) + "-"

        cadena += str(self.cheapest[len(self.cheapest)-1][0]+1) + \
            "-" + str(self.cheapest[len(self.cheapest)-1][1]+1)

        return cadena

    def cheapest_insertion(self, initial_client):
        """Function that gets the best insertion point in a route"""

        # Step 1: Select initial i

        # Step 2: Select j
        minimum = max(self.matrix[initial_client])

        # Step 2.1
        for i in range(len(self.matrix)):
            if(self.matrix[initial_client][i] != 0 and self.matrix[initial_client][i] < minimum):
                minimum = self.matrix[initial_client][i]
                b = i

        self.cheapest = [[initial_client, b], [b, initial_client]]
        self.missing = []
        k = len(self.matrix)-2

        for i in range(len(self.matrix)):
            if(i != initial_client and i != b):
                self.missing.append(i)

        # Step 3: Get insertion points, get minor delta.
        self.cheapest, self.missing = self.getMinusDelta()

        # Step 4: Is it TSP? yes = finish, no = repeat Step 3
        while(len(self.cheapest) != len(self.matrix)):
            self.cheapest, self.missing = self.getMinusDelta()

        return self.cheapest

    def printDistanceMatrix(self):
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix)):
                print(f'{self.matrix[i][j]}\t', end="")
            print()

        return

    def getSavingsMatrix(self):
        """DON'T WORK!!!"""
        n = len(self.matrix)
        savings = []

        for i in range(1, n):
            temp = []
            for j in range(i+1, n):
                temp_int = self.matrix[i][0] + \
                    self.matrix[0][j] - self.matrix[i][j]
                temp.append(round(temp_int, 3))
            savings.append(temp)

        return savings


# Local Functions -----------------------------------------------------------------


def distance(p0: tuple, p1: tuple):
    """Function that returns the distance between 2 points"""

    return round(math.sqrt((int(p0[0]) -
                            int(p1[0]))**2 + (int(p0[1]) - int(p1[1]))**2), 3)


# Main ----------------------------------------------------------------------------


def main():
    pass


# Fin del Main --------------------------------------------------------------------------------


if(__name__ == "__main__"):
    main()
