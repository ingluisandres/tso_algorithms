__license__ = ""
__author__ = "Luis Andrés García Contreras"
__startdate__ = "2/12/2020"
__name__ = "TSO_Algorithms "
__module__ = "sweep_method"
__python_version__ = "3.8.5"

__lastdate__ = ""
__version__ = ""


__all__ = ['']


# Modules ---------------------------------------------------------------------------------
import cheapest_insertion as cheap

import math
import time

# Local Functions --------------------------------------------------------------------------


def getAngle(a, b, c):
    """
    This method achieves the angle between 3 different points.

    Este metodo consigue el angulo entre 3 distintos puntos.
    """

    ang = math.degrees(math.atan2(
        c[1]-b[1], c[0]-b[0]) - math.atan2(a[1]-b[1], a[0]-b[0]))
    return (ang + 360 if ang < 0 else ang)


def getAllAngles(lista, depot, initial_point=(20, 0)):
    """
    This method gets the angles of all clients.

    Este metodo consigue los angulos de todos los clientes.
    """

    for i in range(1, len(lista) + 1):
        temp = getAngle((int(initial_point[0]), int(initial_point[1])),
                        depot,
                        (int(lista[i][0][0]), int(lista[i][0][1])))
        lista[i].append(round(temp, 3))
    return lista


def getPointsAndDemand(file):
    """
    Function that reads a txt file and gets the coordinates of the 
    clients and their demands.

    Función que lee un archivo txt y obtiene las coordenadas de los 
    clientes y sus demandas.
    """

    clients_points = {}
    f = open(file, "r")

    # Read vehicle's capacity
    capacity = int(f.readline())

    # Read location of depot
    depot = f.readline()
    depot = depot.replace("\n", "")
    depot = depot.split(" ")
    depot[0] = int(depot[0])
    depot[1] = int(depot[1])
    depot = tuple(depot)

    # Read clients's number
    n = int(f.readline())

    # Read locations of all clients
    for i in range(1, n+1):
        temp = f.readline()
        temp = temp.replace("\n", "")
        temp = temp.split(" ")
        temp = tuple(temp)
        clients_points[i] = [temp]

    # Read Demand of all clients
    temp = f.readline()
    temp = temp.replace("\n", "")
    temp = temp.split(" ")
    for i in range(1, n+1):
        clients_points[i].append(temp[i-1])

    f.close()

    return clients_points, capacity, depot


def getDistanceMatrix(clients_points, clients_keys):
    """Function that with the coordinates of the clients, makes the distance matrix"""

    tabla_de_distancias = []
    for i in range(len(clients_points)):
        temp = []
        for j in range(len(clients_points)):

            if(clients_points[clients_keys[i]][0] == clients_points[clients_keys[j]][0]):
                temp.append(0)
            else:
                temp.append(cheap.distance(clients_points[clients_keys[i]][0],
                                           clients_points[clients_keys[j]][0]))

        tabla_de_distancias.append(temp)

    return tabla_de_distancias


def getSortedDict(clients, reverse):

    # step 1
    temp_list = []

    for i in range(1, len(clients)+1):
        temp = clients[i][2]
        temp_list.append(temp)

    temp_list = sorted(temp_list, reverse=reverse)

    # step 2
    temp_dict = {}
    for i in range(1, len(clients)+1):
        for j in range(1, len(clients)+1):

            if(temp_list[i-1] == clients[j][2]):
                temp_dict[j] = clients[j]

    return temp_dict


def sweep_method(veh_capacity, clients, clients_keys, x=1, paths=[]):

    while(x <= len(clients_keys)):
        max_capacity = veh_capacity
        chain = ""
        demanda_cumplida = 0

        for i in range(x, len(clients_keys)+2):

            if(i == len(clients_keys)+1):
                temp = printRoute(chain)
                paths.append(temp)
                return paths

            if(demanda_cumplida <= max_capacity):

                if (demanda_cumplida <= max_capacity -
                        int(clients[clients_keys[i-1]][1])):
                    demanda_cumplida += int(clients[clients_keys[i-1]][1])
                    chain += str(clients_keys[i-1]) + "-"
                    x += 1

                else:
                    # x+=i-1
                    temp = printRoute(chain)
                    paths.append(temp)
                    break

        return sweep_method(max_capacity, clients, clients_keys, x, paths)


def printRoute(chain):
    """Function that prints the actual route"""
    return "0-" + chain + "0"


def IsItTSP(clients, sweep):
    ls = []
    temp_list = []

    for i in range(len(sweep)):
        temp = sweep[i].split("-")
        temp.remove("0")
        temp.remove("0")

        temp_dict = {}

        if(len(temp) > 2):
            for j in range(len(temp)):
                temp_dict[int(temp[j])] = clients[int(temp[j])]

            temp_dict[0] = [("0", "0"), "0", 0]
            ls.append(temp_dict)
            temp_list.append(i)

            del sweep[i]

            clients_keys = list(temp_dict.keys())
            matrix = getDistanceMatrix(temp_dict, clients_keys)
            cheapest = cheap.cheapest_insertion(matrix, len(temp_dict)-1)
            sweep.insert(i, printRouteTSP(cheapest, clients_keys))

    return ls, temp_list


def printRouteTSP(ruta, clients_keys):

    cadena = ""

    for i in range(len(ruta)-1):

        cadena += str(ruta[i][0]) + "-"

    cadena += str(ruta[len(ruta)-1][0]) + "-" + str(ruta[len(ruta)-1][1])

    return cadena


def updateSweep(dict_tsp, x, sweep):
    # se hace para cada TSP escenario
    for i in range(len(x)):
        temp = sweep[x[i]].split("-")
        dict_keys = list(dict_tsp[i].keys())
        dict_keys.append(0)

        chain = ""

        # se construye la ruta
        for j in range(len(dict_keys)-1):
            for k in range(len(dict_keys)):
                if(int(temp[j]) == k):
                    chain += str(dict_keys[k]) + "-"
                    break
        chain += "0"

        del sweep[x[i]]
        sweep.insert(x[i], chain)
    return sweep


def getMatrixWithOrigin(clients, depot, is_clock_wise):
    """
    This method returns the matrix of distances of the clients between 
    themselves and between the deposit.

    Este metodo retorna la matriz de distancias de los clientes entre 
    si y entre el deposito.
    """
    temp_list = []
    for i in range(1, len(clients)+1):
        temp = cheap.distance(clients[i][0], depot)
        temp_list.append(temp)

    sorted_clients = getSortedDict(clients, is_clock_wise)
    clients_keys = list(sorted_clients.keys())
    matrix = getDistanceMatrix(clients, clients_keys)
    matrix.insert(0, temp_list)

    for i in range(len(matrix)):
        matrix[i].insert(0, temp_list[i])

    return matrix


def getClientsCost(sweep, matrix):
    """
    This method returns the cost of the routes in a VRP.

    Este metodo retorna el coste de las rutas en un VRP.
    """

    # getSweepUtil
    sweep_util = []
    for i in range(len(sweep)):
        temp = sweep[i].replace("-", " ")
        temp2 = temp.split(" ")
        sweep_util.append(temp2)

    # tempFunct
    temp_list = []
    for j in range(len(sweep_util)):
        temp_list2 = []
        for i in range(len(sweep_util[j]) - 1):
            temp = matrix[int(sweep_util[j][i])][int(sweep_util[j][i+1])]
            temp_list2.append(temp)
        temp_list.append(temp_list2)

    # joinRoutes
    temp = []
    for i in range(len(temp_list)):
        for j in temp_list[i]:
            temp.append(j)

    # getClientCost
    coste_de_espera = 0
    clients = len(temp)

    for i in range(len(temp)):
        resta = clients-i
        coste_de_espera += temp[i] * resta

    return round(coste_de_espera, 3)


def getSweep(clients, is_clock_wise, veh_capacity):
    """
    This function gives you all the routes that a vehicle must follow 
    in the VRP, by the sweep method.

    Esta funcion te da todas las rutas que debe seguir un vehiculo en
    el VRP, por el metodo sweep.
    """

    sorted_clients = getSortedDict(clients, is_clock_wise)
    clients_keys = list(sorted_clients.keys())
    sweep = sweep_method(veh_capacity, sorted_clients, clients_keys, 1, [])
    dict_tsp, x = IsItTSP(clients, sweep)
    updateSweep(dict_tsp, x, sweep)

    return sweep


def printData(sweep, matrix, start_time):
    total_cost = getClientsCost(sweep, matrix)
    for i in range(len(sweep)):
        print(f'Path#{i+1}: {sweep[i]}')
    print(f'Total Cost: {total_cost}')
    seconds = round(time.time() - start_time, 5)
    print("Seconds: %s" % (seconds))  # FIN DE TIEMPO DE EJECUCION
    return


def getAllSweeps(clients, veh_capacity, depot):
    """
    This method does the sweep method several times, as many times as 
    the number of clients times 2.

    Este metodo hace varias veces el sweep method, tantas veces como 
    numero de clientes por 2.
    """

    minimum = 1_000_000
    temp = 0
    """clock_wise = []
    not_clock_wise = []"""

    for i in range(1, len(clients)+1):

        clients = getAllAngles(clients, depot, clients[i][0])  # clients[i][0]

        # True
        clock_wise = getSweep(clients, True, veh_capacity)
        matrix1 = getMatrixWithOrigin(clients, depot, True)
        cost_clock_wise = getClientsCost(clock_wise, matrix1)
        # False
        not_clock_wise = getSweep(clients, False, veh_capacity)
        matrix2 = getMatrixWithOrigin(clients, depot, False)
        cost_not_clock_wise = getClientsCost(not_clock_wise, matrix2)

        if(cost_clock_wise < cost_not_clock_wise):
            temp = cost_clock_wise
            matrix = matrix1

            if(temp < minimum):
                paths = clock_wise
                minimum = temp
        else:
            temp = cost_not_clock_wise
            matrix = matrix2

            if(temp < minimum):
                paths = not_clock_wise
                minimum = temp

        for i in range(1, len(clients)+1):
            del clients[i][2]

    return paths, matrix

# Main ------------------------------------------------------------------------------------


def main():
    start_time = time.time()  # INICIO DE TIEMPO DE EJECUCION

    clients, veh_capacity, depot = getPointsAndDemand("VRPNC.TXT")

    paths, matrix = getAllSweeps(clients, veh_capacity, depot)

    """clients = getAllAngles(clients, depot) 
    is_clock_wise = True
    matrix = getMatrixWithOrigin(clients, depot, is_clock_wise)
    paths = getSweep(clients, is_clock_wise, veh_capacity)"""

    printData(paths, matrix, start_time)

# Fin del Main --------------------------------------------------------------------------------


if(__name__ == "__main__"):
    main()
