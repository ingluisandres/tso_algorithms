import math
import unittest

import cheapest_insertion as cheap

# Classes ---------------------------------------------------------------------------


class test_cheapest_insertion_getTotalCost(unittest.TestCase):

    def test_valid_type(self):
        test_tsp = cheap.TSP("data/TSP.txt", 4)
        test_result = test_tsp.getTotalCost()
        expected_result = 598.082
        self.assertEqual(test_result, expected_result)


class test_cheapest_insertion_printRoute(unittest.TestCase):

    def test_valid_type(self):
        test_tsp = cheap.TSP("data/TSP.txt", 4)
        test_result = test_tsp.printRoute()
        expected_result = '5-12-4-13-15-3-6-11-10-14-9-1-2-8-7-5'
        self.assertEqual(test_result, expected_result)


# -----------------------------------------------------------------------------------

if(__name__ == '__main__'):
    unittest.main()
